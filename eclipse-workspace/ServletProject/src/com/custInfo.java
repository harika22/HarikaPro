package com;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.javatraining.customer.dao.CustomerDAO;
import com.javatraining.customer.impl.CustomerDAOImpl;
import com.javatraining.customer.model.Customer;

/**
 * Servlet implementation class custInfo
 * @param <Customer>
 */
public class custInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public custInfo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id=Integer.parseInt(request.getParameter("customerId"));
		String name=request.getParameter("customerName");
		String address=request.getParameter("customerAddress");
		int amount=Integer.parseInt(request.getParameter("billAmount"));
		
		Customer customer=new Customer(id, name, address, amount);
		CustomerDAO customerDAO=new CustomerDAOImpl();
		int result=customerDAO.insertCustomer(customer);
		
		
		response.getWriter().println("<h2> UserId is-"+id+"</h2>");
		response.getWriter().println("<h2> CustomerName is-"+name+"</h2>");
		response.getWriter().println("<h2> CustomerAddress is-"+address+"</h2>");
		response.getWriter().println("<h2> BillAmount is-"+amount+"</h2>");

				
		response.getWriter().println(result+" rows affected.");
		
		int customerid=Integer.parseInt(request.getParameter("customerId"));
		CustomerDAO dao=new CustomerDAOImpl();
		int rows=dao.deleteCustomer(customerid);
		System.out.println(rows+" deleted.");
		
	}

}
