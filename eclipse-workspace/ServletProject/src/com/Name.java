package com;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Name
 */
public class Name extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Name() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name=request.getParameter("Name");
		String color[]=request.getParameterValues("colors");
		
		if(color!=null)
		{
			for(String col: color)
			{
				request.getParameter(col);
				response.getWriter().println("<font color="+col+">Hello" + name+"</font><br>");
					}
			RequestDispatcher dispatcher=request.getRequestDispatcher("Colors.html");
			dispatcher.include(request, response);
			}
		else
		{
			response.getWriter().println("<No color selected <br>");
			RequestDispatcher dispatcher=request.getRequestDispatcher("Colors.html");
			dispatcher.include(request, response);

		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
