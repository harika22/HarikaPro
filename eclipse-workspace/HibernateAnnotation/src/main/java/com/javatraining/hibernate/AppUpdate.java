package com.javatraining.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.javatraining.customer.model.Customer;

public class AppUpdate {

	
	 public static void main( String[] args ) 
	    {
	     Customer customer=new Customer(1790,"Rishab","Banglore",7788);
	     
//	     this will search for hibernate.cfg.xml and load the db configuration
	     Configuration configuration=new Configuration().configure();
	      SessionFactory factory=configuration.buildSessionFactory();
	      Session session=factory.openSession();
	     Transaction transaction=session.beginTransaction();
	     
	     
	     session.update(customer);
	     transaction.commit();
	     System.out.println("Customer updated");
	     session.close();
	     factory.close();
	    	
	    }
	}


