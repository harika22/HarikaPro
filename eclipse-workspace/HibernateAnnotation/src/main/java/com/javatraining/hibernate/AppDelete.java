package com.javatraining.hibernate;

import java.sql.SQLException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.javatraining.customer.model.Customer;

public class AppDelete {

	/**
	 * Hello world!
	 *
	 */
	
	    public static void main( String[] args ) 
	    {
	     Customer customer=new Customer();
	     customer.setCustomerId(1791);
	     
//	     this will search for hibernate.cfg.xml and load the db configuration
	     Configuration configuration=new Configuration().configure();
	      SessionFactory factory=configuration.buildSessionFactory();
	      Session session=factory.openSession();
	     Transaction transaction=session.beginTransaction();
	     
	     
	     session.delete(customer);
	     transaction.commit();
	     System.out.println("Customer deleted");
	     session.close();
	     factory.close();
	    	
	    }
	}

