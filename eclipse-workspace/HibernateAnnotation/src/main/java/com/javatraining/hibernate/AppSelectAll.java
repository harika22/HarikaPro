package com.javatraining.hibernate;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;

import com.javatraining.customer.model.Customer;


public class AppSelectAll {

	/**
	 * Hello world!
	 *
	 */
	
	    public static void main( String[] args ) 
	    {
	     Customer customer=new Customer();
//	     customer.setCustomerId(1789);
	     
//	     this will search for hibernate.cfg.xml and load the db configuration
	     Configuration configuration=new Configuration().configure();
	      SessionFactory factory=configuration.buildSessionFactory();
	      Session session=factory.openSession();
	      
	      System.out.println("All Customer Details");
	      
//	      Query q=session.createQuery("from Customer");
	      // Query q=session.createQuery("from Customer where customerAddress like 'Mumbai'");
	      //Query q=session.createQuery("from Customer where customerId=1789");
	      
	     Criteria q=session.createCriteria(Customer.class).add(Restrictions.eq("customerId", 1789)).add(Restrictions.gt("billAmount", 7000));
	      
	      List<Customer>customers=q.list();
	      
	     Iterator<Customer>iterator=customers.iterator();
	      while(iterator.hasNext())
	      {
	    	  Customer cust=iterator.next();
	    	  System.out.println(cust);
	      }
	    	  
	      session.close();
		     factory.close();
	    	
	    }
	}
