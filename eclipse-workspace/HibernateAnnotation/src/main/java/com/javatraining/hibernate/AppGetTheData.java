package com.javatraining.hibernate;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.javatraining.customer.model.Customer;

/**
 * Hello world!
 *
 */
public class AppGetTheData 
{
    public static void main( String[] args ) throws SQLException, ClassNotFoundException
    {
     Customer customer=new Customer(1789, "Kohli", "Punjab", 9900);
     
//     this will search for hibernate.cfg.xml and load the db configuration
     Configuration configuration=new Configuration().configure();
     
     SessionFactory factory=configuration.buildSessionFactory();
     
     Session session=factory.openSession();
     
     Transaction transaction=session.beginTransaction();
     session.save(customer);
     transaction.commit();
     System.out.println("Data Stored");
     session.close();
     factory.close();
    	
    }
}
