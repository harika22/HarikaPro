package com.javatraining.customer.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

	@Entity
	@Table(name="cust100")
public class Customer implements Serializable 
{
	
	private static final long serialVersionUID=6111526875639600898L;
	@Id
	private int customerId;
	@Column
	private String CustomerName;
	@Column
	private String CustomerAddress;
	@Column
	private int billAmount;
	
	public Customer() 
	{
	}
	
	

	public Customer(int customerId, String customerName, String customerAddress, int billAmount) {
		super();
		this.customerId = customerId;
		CustomerName = customerName;
		CustomerAddress = customerAddress;
		this.billAmount = billAmount;
	}



	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return CustomerName;
	}

	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}

	public String getCustomerAddress() {
		return CustomerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		CustomerAddress = customerAddress;
	}

	public int getBillAmount() {
		return billAmount;
	}

	public void setBillAmount(int billAmount) {
		this.billAmount = billAmount;
	}



	@Override
	public String toString() {
		return "Customer [customerId=" + customerId + ", CustomerName=" + CustomerName + ", CustomerAddress="
				+ CustomerAddress + ", billAmount=" + billAmount + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + billAmount;
		result = prime * result + ((CustomerAddress == null) ? 0 : CustomerAddress.hashCode());
		result = prime * result + customerId;
	    result = prime * result + ((CustomerName == null) ? 0 : CustomerName.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		if (CustomerAddress == null) {
			if (other.CustomerAddress != null)
				return false;
		} else if (!CustomerAddress.equals(other.CustomerAddress))
			return false;
		if (CustomerName == null) {
			if (other.CustomerName != null)
				return false;
		} else if (!CustomerName.equals(other.CustomerName))
			return false;
		if (billAmount != other.billAmount)
			return false;
		if (customerId != other.customerId)
			return false;
		return true;
	}



	public int compareTo(Customer o) {
		// TODO Auto-generated method stub
		return 0;
	}

	}
	

