package com.javatraining.customer.client;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


import com.javatraining.Config.AppConfig;
import com.javatraining.customer.model.BankAccount;
import com.javatraining.customer.model.Customer;

public class Client {

	public static void main(String[] args) {
		
		ApplicationContext context=new AnnotationConfigApplicationContext(AppConfig.class);
		
		Customer customer1=context.getBean(Customer.class);
		customer1.setCustomerId(22);
		customer1.setCustomerName("Vijay");
		customer1.setCustomerAddress("Hyderabad");
		customer1.setBillAmount(900);
	
		BankAccount bankAcount=context.getBean(BankAccount.class);
		bankAcount.setAccountNumber("321456");
		bankAcount.setBalance(6789);
//		customer1.setBankAccount(bankAcount);
		
		System.out.println(customer1);
		
}
}