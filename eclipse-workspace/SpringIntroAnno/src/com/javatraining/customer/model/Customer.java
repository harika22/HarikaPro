package com.javatraining.customer.model;

import java.io.Serializable;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

public class Customer implements Serializable 
{
	private int customerId;
	private String CustomerName;
	private String CustomerAddress;
	private int billAmount;
	
	@Autowired
	private BankAccount bankAccount;

	
	@PostConstruct
	public void vj() {
		System.out.println("I am Vj");
	}
	
	@PostConstruct
	public void harika() {
		System.out.println("I am Harika");
	}
	
	
	public Customer() {
		super();
	}

	




	public Customer(BankAccount bankAccount) {
		super();
		this.bankAccount = bankAccount;
	}






	public Customer(int customerId, String customerName, String customerAddress, int billAmount) {
		super();
		this.customerId = customerId;
		this.CustomerName = customerName;
		this.CustomerAddress = customerAddress;
		this.billAmount = billAmount;
		System.out.println("In parameterized constructor");
	}





	public Customer(int customerId, String customerName, String customerAddress, int billAmount,
			BankAccount bankAccount) {
		super();
		this.customerId = customerId;
		this.CustomerName = customerName;
		this.CustomerAddress = customerAddress;
		this.billAmount = billAmount;
		this.bankAccount = bankAccount;
	}





	public BankAccount getBankAccount() {
		return bankAccount;
	}


	public void setBankAccount(BankAccount bankAccount) {
		this.bankAccount = bankAccount;
	}





	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return CustomerName;
	}

	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}

	public String getCustomerAddress() {
		return CustomerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		CustomerAddress = customerAddress;
	}

	public int getBillAmount() {
		return billAmount;
	}

	public void setBillAmount(int billAmount) {
		this.billAmount = billAmount;
	}



	@Override
	public String toString() {
		return "Customer [customerId=" + customerId + ", CustomerName=" + CustomerName + ", CustomerAddress="
				+ CustomerAddress + ", billAmount=" + billAmount + ", bankAccount=" + bankAccount + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + billAmount;
		result = prime * result + ((CustomerAddress == null) ? 0 : CustomerAddress.hashCode());
		result = prime * result + customerId;
	    result = prime * result + ((CustomerName == null) ? 0 : CustomerName.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		if (CustomerAddress == null) {
			if (other.CustomerAddress != null)
				return false;
		} else if (!CustomerAddress.equals(other.CustomerAddress))
			return false;
		if (CustomerName == null) {
			if (other.CustomerName != null)
				return false;
		} else if (!CustomerName.equals(other.CustomerName))
			return false;
		if (billAmount != other.billAmount)
			return false;
		if (customerId != other.customerId)
			return false;
		return true;
	}



//	@Override
//	public int compareTo(Customer o) {
//		// TODO Auto-generated method stub
//		return 0;
	}
	
	
	

