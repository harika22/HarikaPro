package com.javatraining.customer.model;

public class BankAccount {
	private String accountNumber;
	private int balance;
	
	public BankAccount() {
		
	}

	public BankAccount(String accountNumber, int balance) {
		super();
		this.accountNumber=accountNumber;
		this.balance=balance;
	}
	
	public String getAccountNumber() {
		return accountNumber;
		}

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	@Override
	public String toString() {
		return "BankAccount [accountNumber=" + accountNumber + ", balance=" + balance + "]";
	}
	
}
