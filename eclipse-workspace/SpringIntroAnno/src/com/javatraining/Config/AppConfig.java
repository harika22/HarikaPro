package com.javatraining.Config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import com.javatraining.customer.model.BankAccount;
import com.javatraining.customer.model.Customer;


public class AppConfig {
	
	@Bean
	@Scope("prototype")
	public Customer getCustomerObject()
	{
		BankAccount account=this.getBankAccount();
		return new Customer(account);
	}
	
	@Bean
	public BankAccount getBankAccount()
	{
		return new BankAccount();
	}

}
