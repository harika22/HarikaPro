package com.javatraining.customer.client;

import java.util.ArrayList;
import java.util.List;

import com.javatraining.customer.dao.CustomerDAO;
import com.javatraining.customer.impl.CustomerDAOImpl;
import com.javatraining.customer.model.Customer;

public class Client {

	public static void main(String[] args) {

		Customer customer=new Customer(2,"harika","Delhi",700);
		
		CustomerDAO customerDAO=new CustomerDAOImpl();
		
//		testing the method for insert
//		int result=customerDAO.insertCustomer(customer);
//		System.out.println(result+" rows affected.");
//		
		
//		
//		testing the method for update
//		int customerId=999;
//		String newCustomerAddress="Agra";
//		int newBillAmount=5900;
//		int result=customerDAO.updateCustomer(customerId, newCustomerAddress, newBillAmount);
//		System.out.println(result+" rows updated.");
		
		
		//testing the listall Method
		List<Customer> allcustomers=new ArrayList<Customer>();
		allcustomers=customerDAO.listAllCustomers();
		System.out.println(allcustomers);
		
//		
//		//testing delete customer
//		int customerId=2;
//		CustomerDAO dao=new CustomerDAOImpl();
//		int rows=dao.deleteCustomer(customerId);
//		System.out.println(rows+" deleted.");
//		
		
		
//		//tetsing customer exists
//		CustomerDAO dao=new CustomerDAOImpl();
//		if (dao.isCustomerExists(2))
//		{
//			System.out.println("Exixts");
//			System.out.println(dao.findByCustomerId(2));
//		}
//		else
//		{
//			System.out.println("Does not exist.");
		}
		
	}


