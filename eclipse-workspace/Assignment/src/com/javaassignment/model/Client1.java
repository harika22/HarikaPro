package com.javaassignment.model;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;


public class Client1 {
	public static void main(String[] args) {
		Resource resource= new ClassPathResource("beans.xml");
		BeanFactory	factory=new XmlBeanFactory(resource);
		Email email=(Email)factory.getBean("email");
		System.out.println(email);

	}

}
