package com.javaassignment.model;

public class Subject {

	private String caption;

	public Subject() {
		super();
	}

	public Subject(String caption) {
		super();
		this.caption = caption;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}
}
