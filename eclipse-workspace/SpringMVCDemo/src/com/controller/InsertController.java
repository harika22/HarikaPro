package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.javatraining.customer.dao.CustomerDAO;
import com.javatraining.customer.impl.CustomerDAOImpl;
import com.javatraining.customer.model.Customer;
import com.javatraining.customer.service.CustomerService;
import com.model.Guest;

@Controller
public class InsertController {
	
	@Autowired
	CustomerService customerservice;
	
	@RequestMapping("/Insert")
	public ModelAndView getA()
	{
		ModelAndView view= new ModelAndView();
		view.setViewName("in");
		view.addObject("message","Good Morning from Controller");
		return view;
	}

	@RequestMapping("/guest")
	public ModelAndView getGuestView(Guest guest)
	{
		ModelAndView view= new ModelAndView();
		view.setViewName("guestDetails");
		view.addObject("message","Good Morning from Controller");
		view.addObject("guestInfo",guest);

		return view;
	}
	
	
	@RequestMapping("/customerDetails")
	public ModelAndView getCustomeriew(Customer customer)
	{
//		CustomerDAO dao =new CustomerDAOImpl();
		customerservice=insertCustomer(customer);
		ModelAndView view= new ModelAndView();
		view.setViewName("customerDetails");
		view.addObject("message",customer.getCustomerName()+ " Your record is saved successfully");

		return view;
	}

	private CustomerService insertCustomer(Customer customer) {
		// TODO Auto-generated method stub
		return null;
	}
}
