package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.javatraining.customer.model.Customer;

@Controller
public class IndexController {

	@RequestMapping("/CustomerF")
	public ModelAndView getA()
	{
		ModelAndView view= new ModelAndView();
		view.setViewName("customerForm");
		view.addObject("custp", new Customer());
		return view;
	}
}
