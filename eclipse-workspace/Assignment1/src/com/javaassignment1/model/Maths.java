package com.javaassignment1.model;

public class Maths {

	private int Mmarks;
	private String Mgrade;
	public int getMmarks() {
		return Mmarks;
	}
	public void setMmarks(int mmarks) {
		Mmarks = mmarks;
	}
	public String getMgrade() {
		return Mgrade;
	}
	public void setMgrade(String mgrade) {
		Mgrade = mgrade;
	}
	@Override
	public String toString() {
		return "Maths marks = "  + Mmarks + ", grade = " + Mgrade;
	}
	public Maths(int mmarks, String mgrade) {
		super();
		Mmarks = mmarks;
		Mgrade = mgrade;
	}
	
	
	
}
