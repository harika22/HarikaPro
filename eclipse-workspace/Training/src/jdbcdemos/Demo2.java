package jdbcdemos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.javatraining.dbcon.DBConfig;

public class Demo2 {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {

		Connection connection= DBConfig.getConnection();
		
		Statement statement=connection.createStatement();
		ResultSet res=statement.executeQuery("select * from customer");
		
		while(res.next())
		{
			System.out.print(res.getString(1)+" ");
			System.out.print(res.getString(2)+" ");
			System.out.print(res.getString(3)+" ");
			System.out.println(res.getString(4)+" ");

		}
		
		res.close();
		statement.close();
		connection.close();
	}
}
