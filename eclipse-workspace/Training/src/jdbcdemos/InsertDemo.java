package jdbcdemos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.javatraining.dbcon.DBConfig;
import p1.Read;

public class InsertDemo {

	public static void main(String[] args) throws SQLException {
		System.out.println("Enter customer ID");
		int cId = Read.sc.nextInt();
		
		System.out.println("Enter customer Name");
		String cName =Read.sc.next();
		
		System.out.println("Enter customer Address");
		String cAdd =Read.sc.next();
		
		System.out.println("Enter bill amount");
		int bAmt = Read.sc.nextInt();
		
		
		Connection conn = DBConfig.getConnection();
		PreparedStatement statement = conn.prepareStatement("insert into customer values(?,?,?,?)");
		statement.setInt(1, cId);
		statement.setString(2, cName);
		statement.setString(3, cAdd);
		statement.setInt(4, bAmt);
		
		int rows = statement.executeUpdate();
		
		System.out.println(rows + "Updated");
		

	}

}
