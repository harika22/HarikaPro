package jdbcdemos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.javatraining.dbcon.DBConfig;

import p1.Read;

public class Demo3 {
	
	public static void main(String[] args) throws SQLException {
		
		Connection conn = DBConfig.getConnection();
		
		System.out.println("Enter customerId");
		int cId=Read.sc.nextInt();
		System.out.println("Enter customer Address");
		String cAdd =Read.sc.next();
		
		System.out.println("Enter bill amount");
		int bAmt = Read.sc.nextInt();
		
		
		String query= "update customer set customerAddress = ?, billAmount=? where customerId=?";
		
		PreparedStatement statement = conn.prepareStatement(query);
		statement.setString(1, cAdd);
		statement.setInt(2, bAmt);
		statement.setInt(3, cId);
		int rows = statement.executeUpdate();
		System.out.println(rows + "Updated");
		
		conn.close();
	}

}
