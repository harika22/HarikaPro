package jdbcdemos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.javatraining.dbcon.DBConfig;

public class User {

	public static void main(String[] args) throws SQLException {
		
		Scanner scanner=new Scanner(System.in);
		
		System.out.println("Enter the User Name: ");
		String username=scanner.next();
		
		
		System.out.println("enter the password: ");
		int password=scanner.nextInt();
		
		Connection connection = DBConfig.getConnection();
		
		String query="select * from users where userName=? AND password=?";
		
		PreparedStatement statement= connection.prepareStatement(query);
		statement.setString(1, username);
		statement.setInt(2, password);
		
		ResultSet res= statement.executeQuery();
		
		if(res.next())
		{
		System.out.println("user name is valid");
		}
		else 
		{
			System.out.println("user name is invalid");
		}
		connection.close();
		res.close();
		statement.close();
		
}
}
