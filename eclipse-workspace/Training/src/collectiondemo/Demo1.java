package collectiondemo;
	
	import java.util.TreeSet;
	
public class Demo1 {
	
	public static void main(String[]args) {
		
		TreeSet<String> set=new TreeSet<String>();
		
		set.add("Reshma");
		set.add("Harika");
		set.add("Yashmin");
		set.add("Maha");
		
		System.out.println(set);
	}

}
