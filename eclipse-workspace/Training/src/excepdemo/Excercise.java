package excepdemo;

public class Excercise {
	public static void main(String[]args) {
		String str="The quick brown fox jumps over the lazy dog";
		
		System.out.println("character at 12th index: "+str.charAt(12));
		System.out.println("Check whether the String contains the word �is�: "+str.contains("is"));
		str=str.concat(" and killed it");
		System.out.println("\n Add the string �and killed it� to the existing string: "+str);
		System.out.println("Check whether the String ends with the word �dogs�: "+ str.endsWith("dogs"));
		System.out.println("Check whether the String is equal to �The quick brown Fox jumps over the lazy Dog�: "+str.equals("The quick brown fox jumps over the lazy dog"));
		System.out.println("Check whether the String is equal to �THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG�.: "+str.equals("The quick brown fox jumps over the lazy dog"));
		System.out.println(" the length of the String: "+str.length());
		System.out.println("Check whether the String matches to �The quick brown Fox jumps over the lazy Dog�.: "+str.compareTo("The quick brown fox jumps over the lazy dog"));
		System.out.println("Replace the word �The� with the word �A�: "+str.replace("The", "A"));
		System.out.println();
		
		
	}
}
