package excepdemo;

public class Demo4 {

	public static void main(String[]args) {
		String marks="98";
		// primitive<--object
		int m=Integer.parseInt(marks);
		System.out.println(m+10);
		
		//primitive-->object(Boxing)
		int num=10;
		Integer n=new Integer(num);   //Boxing
		Integer num1=num;             //Auto Boxing
		
		//object-->primitive(Unboxing)
		Integer scores=109;
		int i=Integer.valueOf(scores);  //Unboxing
		int latestScores=scores;    //Auto Unboxing
		
	}
}
