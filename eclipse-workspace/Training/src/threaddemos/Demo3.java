package threaddemos;

public class Demo3 extends Thread {

	public static synchronized void display(String threadName, String message1,String message2)
	{
		System.out.println("Message by: "+threadName);
		System.out.println("Message 1 is: "+message1);
		
		try 
		{
			Thread.sleep(1000);	
		} 
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		System.out.println("Message2 is: "+message2);
		}
   public Demo3 (int i) 
   {
	super(""+i);
	start();
}
   @Override
	public void run() {
	{
		display(Thread.currentThread().getName(), "hi", "bye");
		System.out.println("RUN CALLED BY:" +Thread.currentThread().getName());
	}
}
	public static void main(String[]args) {
	
		for(int i=0; i<=4;i++)
		{
			new Demo3(i);
	}
		System.out.println("RUN CALLED BY:" +Thread.currentThread().getName());
}
}
	
		

