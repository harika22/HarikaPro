package inheritdemo;

abstract class Gadget {
  int ram=8;
  }
interface Radio
{
		int frequency=82;
		void play();
}
interface ModernRadio extends Radio
{
	
}
interface Apps
{
	
}
class Mobile extends Gadget implements ModernRadio, Apps
{
	@Override
	public void play() {
		System.out.println("ram=" + ram);
		System.out.println("frequency=" +frequency);
		System.out.println("radio played");
}
}
class Execute
{
	public static void main(String[]args) {
		Mobile m=new Mobile();
		m.play();
}}
