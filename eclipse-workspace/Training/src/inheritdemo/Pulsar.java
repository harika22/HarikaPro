package inheritdemo;

class Pulsar extends Bike
{
	public void kickstart() {
		System.out.println("bike kickstarted");

	}

	@Override
	public void stop() {
		System.out.println("bike stopped");
	}

}
