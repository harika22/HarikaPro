package inheritdemo;

public class Main3 {
	public static void main(String[] args)
	{
		Customer customer1= new Customer(134, "harika", "chennai", 22000);
		Customer customer2= new Customer(134, "harika", "chennai", 22000);
		customer1.setCustomerAddress("Bangalore");
		customer2.setCustomerAddress("Bangalore");
		System.out.println();
		
		System.out.println(customer1);
		System.out.println(customer2);
		System.out.println();
		
		System.out.println("customer1.equals(customer2): "+customer1.equals(customer2));
		System.out.println();
		
		System.out.println("Hash code value of customer1: "+customer1.hashCode());
		System.out.println("Hash code value of customer2: "+customer2.hashCode());
		
		
	
			}
}



