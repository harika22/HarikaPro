package inheritdemo;

abstract class Bike extends Vehicle
{
	
	public void start() {
	 
		System.out.println("\nBike started");
	}
	public abstract void kickstart();
}


