package inheritdemo;

 class Car extends Vehicle {
	String carType;
	String color="White";
	public void start()
	{
		System.out.println("car started");
	}
	
	public void showDetails()
	{
		
		noOfWheels=4;
		carType="HatchBack";
				
				System.out.println("Car color is:" +color);
				System.out.println("Car color is:" +super.color);
				System.out.println("Car has noOfWheels:" +noOfWheels);
				System.out.println("CarType is:" +carType);
	}
	
	@Override
	public void stop() {
		System.out.println("car stopped");
	}

}
