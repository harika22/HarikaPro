package p1;

public class Conversion {
	public static String getWords(long n) {
		String word = "";
		String unit[] = {"", "one","two","three","four",
				"five","six","seven","eight","nine","ten","eleven",
				"twelve","thirteen","fourteen","fifteen","Sixteen",
				"seventeen","eighteen","nineteen"};
		String tens[] = {"","","tewnty" , "thirty","fourty","fifty","sixty"
				,"seventy","eighty","ninety"};
		String wval[] = {"crore","lakhs","thousand","hundred","only"};
		long nval[] = {10000000,100000,1000,100,1};
		for(int i =0;i<nval.length; i++) {
			int n1 = (int) (n/nval[i]);
			n = n%nval[i];
			if(n1>0) {
				if(n1>19) {
					word += tens[n1/10] + unit[n1%10] + wval[i];
					
				}else {
					word += unit[n1] + wval[i];
				}
			}
		}
		return word;
		
	}
	
}