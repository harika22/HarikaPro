package p1;

public enum Daydetails {

	Sunday("Sun","Red","Self luminios"),
	Monday("Moon","White"),
	Tuesday("Mars","Yellow_Red"),
	Wednesday("Mercury","Green"),
	Thursday("Jupiter","Yellow"),
	Friday("Venus","White"),
	Saturday("Saturn","Blue");
	String s1,s2,s3="Non_Luminious-bodies";
	private Daydetails(String x, String y) {
		s1 = x;
		s2 = y;
	}
	private Daydetails(String x, String y,String z) {
		s1 = x;
		s2 = y;
		s3 = z;
	}
	
}

