package iodemos;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Demo5 {

	public static void main(String[] args) throws IOException {
		
		BufferedReader buf=new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Please enter your name: ");
		String name=buf.readLine();
		
		System.out.println("Please enter your marks: ");
		int marks=Integer.parseInt(buf.readLine());
		
		System.out.println(name +" scored "+ marks);
		
	}
}
