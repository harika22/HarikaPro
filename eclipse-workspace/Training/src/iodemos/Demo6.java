package iodemos;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class Demo6 {
	 public static void main(String[] args) throws IOException {
		 int salary=76000;
		 boolean married=true;
		 double average=98.8;
		 
		 DataOutputStream stream =new DataOutputStream(new BufferedOutputStream(new FileOutputStream(new File("rec.txt"))));
	
	 stream.writeInt(salary);
	 stream.writeBoolean(married);
	 stream.writeDouble(average);
	 
	 stream.close();
	 System.out.println("Done");
	 }
}
