package iodemos;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Demo3 {
	public static void main(String[] args)throws IOException {
		
		String readFile="data.txt";
		String writeFile="newData.txt";
		 
//		File file=new File("data.txt");
//		FileOutputStream stream=new FileOutputStream(file);
//		
//		String s="welcome to the world";
//		byte b[]=s.getBytes();//converting string into byte array
//		stream.write(b);
//		
//		
//		System.out.println("Success...");
//		stream.close();
//		
//		
//		
//		
//	}
//}

		File fileR=new File(readFile);
		File fileW=new File(writeFile);
		
		FileInputStream readStream=new FileInputStream(fileR);
		FileOutputStream writestream=new FileOutputStream(fileW);
		
		int i=0;
		while((i=readStream.read())!=-1)
		{
			writestream.write((char)i);
		}
		System.out.println("Copied");
		readStream.close();
		writestream.close();
	}
}

		
		