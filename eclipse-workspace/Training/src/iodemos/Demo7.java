package iodemos;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;

public class Demo7 {
	
	public static void main(String[] args) throws IOException {
		 
		DataInputStream stream =new DataInputStream(new BufferedInputStream(new FileInputStream("rec.txt")));
		System.out.println(stream.readInt());
		System.out.println(stream.readBoolean());
		System.out.println(stream.readDouble());
		
		stream.close();
	}

}
