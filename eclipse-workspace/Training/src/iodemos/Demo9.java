package iodemos;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;


public class Demo9 {
	
	public static void main(String[] args) throws FileNotFoundException, IOException {
		Customer customer=new Customer(123,"harika", "delhi", 6500);
		
		ObjectOutputStream stream=new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream("cust.txt")));

		stream.writeObject(customer);
		stream.close();
		System.out.println("Customer Data stored");
}
}