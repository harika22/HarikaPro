package iodemos;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;

public class Demo8 {

	public static void main(String[] args) throws IOException {
		DataInputStream stream =new DataInputStream(new BufferedInputStream(new FileInputStream("rec.txt")));
		int i=stream.readInt();
		boolean b=stream.readBoolean();
		double d=stream.readDouble();
		
		System.out.println("the new salary is:"+(i+2000));
		System.out.println(b);
		System.out.println(d);

	}
}
