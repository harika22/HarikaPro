package com.javatraining.customer.client;


import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.javatraining.customer.model.Customer;

public class Client {

	public static void main(String[] args) {
		
//		Customer customer=new Customer();
//		customer.setCustomerName("Harika");
//		System.out.println("Customer name is: "+customer.getCustomerName());
//		
		Resource resource= new ClassPathResource("beans.xml");
		BeanFactory	factory=new XmlBeanFactory(resource);
		Customer customer=(Customer)factory.getBean("cust1");
		
//		customer.setCustomerName("Harika");
		
		System.out.println(customer);
		
	}
}
