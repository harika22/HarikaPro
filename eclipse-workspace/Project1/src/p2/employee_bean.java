package p2;

public class employee_bean {

	private String emp_name;
	private int emp_age;
	private int salary;
	private int i_tax;
	public String getEmp_name() {
		return emp_name;
	}
	public void setEmp_name(String emp_name) {
		this.emp_name = emp_name;
	}
	public int getEmp_age() {
		return emp_age;
	}
	public void setEmp_age(int emp_age) {
		this.emp_age = emp_age;
	}
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}
	public int getI_tax() {
		return i_tax;
	}
	public void setI_tax(int i_tax) {
		this.i_tax = i_tax;
	}

}
