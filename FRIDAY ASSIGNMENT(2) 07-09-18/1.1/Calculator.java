package test1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Calculator {
	
	public static void main(String[] args) {			
			Arithmetic a[] =new Arithmetic[4];
			
			
			a[0]= new Addition();
			a[1]= new Subtraction();
			a[2]= new Multiplication();
			a[3]= new Division();
			
			Scanner sc = new Scanner(System.in);
			System.out.println("enter 1st number: ");
			double n1 = sc.nextDouble();
			System.out.println("enter 2nd number: ");
			double n2 = sc.nextDouble();
			System.out.println("enter your choice");
			int ch = sc.nextInt();
			
			a[0].read(n1, n2);
			a[1].read(n1, n2);
			a[2].read(n1, n2);
			a[3].read(n1, n2);
			
			a[0].calculate();
			a[1].calculate();
			a[2].calculate();
			a[3].calculate();
			
			try {
				a[--ch].display();
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Invalid Choice");
			}
	
			
		
	}
}